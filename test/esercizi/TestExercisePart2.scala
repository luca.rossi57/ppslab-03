package esercizi

import esercizi.ListsExtended.{foldLeft, getCoursesFromListOfPerson}
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import u02.Modules.Person
import u02.Modules.Person.{Student, Teacher}
import u03.Lists
import u03.Lists.List.{Cons, Nil}

class TestExercisePart2 {

  val listOfPerson:Lists.List[Person] = Cons(Student("mario",2015), Cons(Teacher("Mirko Viroli", "paradigmi ecc"),
    Cons(Teacher("Alessandro Ricci", "programmazione concorrente"), Nil())))

  val lst = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))

  @Test
  def testGetCoursesFromPeopleList() {
    assertEquals(Cons("paradigmi ecc", Cons("programmazione concorrente",Nil())), getCoursesFromListOfPerson(listOfPerson))
    assertEquals(Nil(), getCoursesFromListOfPerson(Cons(Student("Luca",22),Nil())))
    assertEquals(Nil(), getCoursesFromListOfPerson(Nil()))
  }

  @Test
  def testLeftFold() {
    assertEquals(-16  , foldLeft(lst)(0)(_-_))
    //assertEquals(-8, foldRight(lst)(0)(_-_))
  }

}
