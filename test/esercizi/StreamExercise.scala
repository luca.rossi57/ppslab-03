package esercizi

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u03.Lists.List.Cons
import u03.Lists.List.Nil
import u03.Streams.Stream

class StreamExercise {

  val s = Stream.take(Stream.iterate(0)(_+1))(10)

  @Test
  def testDrop(){
    assertEquals(Cons(6, Cons(7, Cons(8, Cons(9, Nil())))), Stream.drop(s)(6))
  }


  @Test
  def testConst(){
    assertEquals(Stream.toList(Stream.cons("x", Stream.cons("x", Stream.cons("x", Stream.cons("x",
      Stream.cons("x", Stream.empty())))))), Stream.take(Stream.constant("x"))(5))
  }

  @Test
  def testFibs(){
    assertEquals(Cons(0, Cons(1, Cons(1, Cons(2 , Cons(3, Cons(5, Cons(8,
      Cons(13, Nil())))))))), Stream.take(Stream.fibs)(8))
  }

}
