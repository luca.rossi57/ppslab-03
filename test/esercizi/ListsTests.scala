package esercizi


import esercizi.ListsExtended.{drop, flatMap, map, filter, max}
import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u03.Lists.List.Cons
import u03.Lists.List.Nil
import u02.Optionals.Option._

class ListsTests {

  val lst = Cons(10, Cons(20, Cons(30, Nil())))

  @Test
  def testDrop() {
    assertEquals(Cons (20, Cons(30, Nil())), drop(lst, 1))
    assertEquals(Cons(30, Nil()), drop(lst, 2))
    assertEquals(Nil(), drop(lst, 5))
  }

  @Test
  def testFlatMap() {
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), flatMap(lst)(v => Cons(v +1, Nil())))
    assertEquals(Cons(11,Cons(12, Cons(21, Cons(22, Cons(31, Cons(32, Nil())))))),
      flatMap(lst)(v => Cons(v +1, Cons(v+2, Nil()))))
  }

  @Test
  def testMapAsFlatMap() {
    assertEquals(Cons("10", Cons("20", Cons("30", Nil()))), map(lst)(v => v.toString))
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), map(lst)(v => v+1))
  }

  @Test
  def testFilterAsFlatMap() {
    assertEquals(Cons (20, Cons(30, Nil())), filter(lst)(_ > 10))
    assertEquals(Cons(30, Nil()), filter(lst)(_ > 20))
    assertEquals(Nil(), filter(lst)(_ > 30))
  }

  @Test
  def testMax() {
    assertEquals(Some(30), max(lst))
    assertEquals(None(), max(Nil()))
  }
}
