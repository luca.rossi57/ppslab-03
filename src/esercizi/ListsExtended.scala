package esercizi

import u02.Modules.Person
import u02.Modules.Person.{Student, Teacher}
import u03.Lists.List.{Cons, Nil, append}
import u03.Lists._
import u02.Optionals.Option.Some
import u02.Optionals.Option.None
import u02.Optionals.Option


object ListsExtended {

    def drop[A](list: List[A], n: Int):List[A] = list match {
      case Cons(_,t) if n>0 => drop(t, n-1)
      case Cons(h,t) if n == 0 => Cons(h,t)
      case Nil() => Nil()
    }

    def flatMap[A,B](l:List[A])(f: A => List[B]): List[B] = l match {
      case Cons(h,t) => append(f(h), flatMap(t)(f))
      case Nil() => Nil()
    }

    def map[A,B](l: List[A])(mapper: A=>B): List[B] =
      flatMap(l)(v => Cons(mapper(v),Nil()))

    def filter[A](l1: List[A])(pred: A=>Boolean): List[A] =
      flatMap(l1)({
        case v if pred(v) => Cons(v, Nil())
        case _ => Nil()
      })


    def max1(l:List[Int]): Option[Int] = ???

    def _max(list: List[Int], maxValue: Int): Option[Int] = list match {
        case Cons(h, t) if h > maxValue => _max(t, h)
        case Cons(_, t) => _max(t, maxValue)
        case Nil() => Some(maxValue)
      }

    def max(l:List[Int]):Option[Int] = l match {
      case Cons(h,t) => _max(t, h)
      case Nil() =>None()
    }

    //exercises part 2
    def getCoursesFromListOfPerson(list: List[Person]): List[String] =
      flatMap(list)({
        case Teacher(_, s) =>Cons(s,Nil())
        case Student(_,_)=> Nil()
      })

    def foldLeft[B](list: List[B])(acc: B)(pred: (B,B) =>B) : B = list match {
      case Cons(h,t) => foldLeft(t)(pred(acc, h))(pred)
      case Nil() => acc
    }

   /* def foldLeftWithGenerics[A, B](list: List[A])(acc: B)(pred: (B,B) =>B) : B = list match {
      case Cons(h,t) => foldLeft(t)(pred(acc, h))(pred)
      case Nil() => acc
    }*/

    def foldRight1[A,B](list: List[A])(acc: B)(pred: (B,B) =>B) = ???


    /*def foldRight[B](list: List[B])(acc: B)(pred: (B,B) =>B): B = list match {
      case Cons(h,t) => pred(h,t)
      case Nil() => acc
    }*/

}